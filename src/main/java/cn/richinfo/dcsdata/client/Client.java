package cn.richinfo.dcsdata.client;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ConnectionlessBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.channel.socket.DatagramChannelFactory;
import org.jboss.netty.channel.socket.nio.NioDatagramChannelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Client {
	private static final Logger logger = LoggerFactory.getLogger(Client.class);
	
	private int terminated = 0; 
	
	private Config config = null;
	
	private List<OpcReader> opcReaders = new ArrayList<>();
	// private List<FileDataWriter> fileWriters = null;
	private List<UdpDataWriter> udpWriters = new ArrayList<>();
	
	private ExecutorService thpool;

	private int initProc() {
		// make sure only 1 proc be running
		InetSocketAddress locAddr = new InetSocketAddress("0.0.0.0", 31000);
		
//		try {
//			@SuppressWarnings({ "unused", "resource" })
//			final DatagramSocket sock = new DatagramSocket(locAddr);
//		} catch (SocketException e) {
//			logger.error(String.format("init sock fail"), e);
//			return -1;
//		}
		
		DatagramChannelFactory channelFactory = new NioDatagramChannelFactory();
		ConnectionlessBootstrap bootstrap = new ConnectionlessBootstrap(channelFactory);
		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
			@Override
			public ChannelPipeline getPipeline() throws Exception {
				return Channels.pipeline(new SimpleChannelUpstreamHandler());
			}
		});
		try {
			@SuppressWarnings("unused")
			Channel channel = bootstrap.bind(locAddr);
		} catch (Exception e) {
			logger.error(String.format("bind fail"), e);
			return -1;
		}
		
		logger.info(String.format("init sock success"));

		return 0;
	}
	
	public int init() {
		int ret = 0;
		
		ret = initProc();
		if (ret < 0) {
			logger.error("init proc fail");
			return -1;
		}
		
		config = new Config();
		ret = config.init();
		if (ret < 0) {
			logger.error("init config fail");
			return -1;
		}
		
		int workNum = config.getWorkNum();
		int itemPerWork = config.listItemGroups().length / workNum + 1; 
		
		for (int seq = 0; seq < workNum; seq++) {
			OpcReader opcReader = new OpcSyncReader();
			opcReader.setHost(config.getOpcSvrHost());
			opcReader.setUser(config.getOpcSvrUser());
			opcReader.setPasswd(config.getOpcSvrPasswd());
			opcReader.setProgId(config.getOpcProgId());
			ret = opcReader.init();
			if (ret < 0) {
				logger.error("init opc reader fail");
				return -1;
			}
			
			UdpDataWriter udpWriter = new UdpDataWriter();
			udpWriter.setLocPort(32000 + seq);
			udpWriter.setDcHost(config.getDwSvrHost());
			udpWriter.setDcPort(config.getDwSvrPort());
			ret = udpWriter.init();
			if (ret < 0) {
				logger.error("init udp writer fail");
				return -1;
			}
			
			for (int i = 0; i < itemPerWork; i++) {
				int idx = seq * itemPerWork + i;
				if (idx >= config.listItemGroups().length) {
					break;
				}
				opcReader.addItemGroup(config.listItemGroups()[idx]);
			}
			
			opcReader.addDataListener(udpWriter);
			
			opcReaders.add(opcReader);
			udpWriters.add(udpWriter);
		}
		
		thpool = Executors.newFixedThreadPool(workNum);
		
		return 0;
	}
	
	public int handle() {
		while (terminated == 0) {
			logger.debug("handle");
			
			for (final OpcReader opcReader: opcReaders) {
				thpool.submit(new Runnable() {
		            @Override
		            public void run() {
		            	long begin = System.currentTimeMillis();
		            	int ret = opcReader.read();
		            	long end = System.currentTimeMillis();
		            	if (ret < 0) {
		            		logger.debug(String.format("read fail, result: %d, time: %dms", ret, end - begin));
		            	} else {
		            		logger.debug(String.format("read success, time: %dms", end - begin));
		            	}
		            }
		        });
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.info("interrupted", e);
				terminated = 1;
			}
		}
		
		return 0;
	}
	
	public static void main(String[] args) {
		Client client = new Client();
		int ret = client.init();
		if (ret < 0) {
			logger.error(String.format("init fail, result: %d", ret));
			System.exit(-1);
		}
		client.handle();
	}
}
