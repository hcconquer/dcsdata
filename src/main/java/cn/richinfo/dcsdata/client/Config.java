package cn.richinfo.dcsdata.client;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.richinfo.dcsdata.opc.ItemDesc;
import cn.richinfo.dcsdata.opc.ItemGroup;

public class Config {
	private static final Logger logger = LoggerFactory.getLogger(Config.class);
	
	private String path = "conf/client.properties";
	
	private String opcSvrHost = "222.222.223.113";
	private String opcSvrUser = "Administrator";
	private String opcSvrPasswd = null;
	private String opcProgId = "Intellution.XOSOPC";
	
	private String svrHost = "106.120.108.105"; // "127.0.0.1"; 
	private int svrPort = 30102;
	
	private int workNum = 16;
	
	private int readDevice = 0;
	private int readAi = 0;
	private int readDi = 0;
	
	private List<ItemGroup> groups = new ArrayList<>();
	
	public Config() {
	}
	
	public Config(String path) {
		this.path = path;
	}
	
	public int init() {
		Properties props = new Properties();
		try {
			props.load(new FileReader(new File(path)));
		} catch (IOException e) {
			logger.error("load config fail", e);
		}
		
		/* opc server */
		opcSvrHost = props.getProperty("opc_svr_host", opcSvrHost);
		opcSvrUser = props.getProperty("opc_svr_user", opcSvrUser);
		opcSvrPasswd = props.getProperty("opc_svr_passwd", opcSvrPasswd);
		opcProgId = props.getProperty("opc_prog_id", opcProgId);
		
		/* data center */
		svrHost = props.getProperty("dw_svr_host", svrHost);
		svrPort = Integer.valueOf(props.getProperty("dw_svr_port", String.valueOf(svrPort)));
		
		workNum = Integer.valueOf(props.getProperty("work_num", String.valueOf(workNum)));
		
		readDevice = Integer.valueOf(props.getProperty("read_device", String.valueOf(readDevice)));
		readAi = Integer.valueOf(props.getProperty("read_ai", String.valueOf(readAi)));
		readDi = Integer.valueOf(props.getProperty("read_di", String.valueOf(readDi)));
		
		logger.info(String.format("opc_svr_host: %s, opc_svr_user: %s, opc_svr_passwd: %s, read_ai: %d, read_di: %d", 
				opcSvrHost, opcSvrUser, opcSvrPasswd, readAi, readDi));
		
		try {
			Collection<File> files = FileUtils.listFiles(new File("conf/items"), new String[] { "json" }, true);
			for (File file: files) {
				ItemGroup group = ItemGroup.load(file);
				
				List<ItemDesc> items = group.getItems();
				for (Iterator<ItemDesc> iter = items.iterator(); iter.hasNext();) {
					ItemDesc item = iter.next();
					if (item.getType() == ItemDesc.TYPE_AI) {
						if (readAi == 0) {
							iter.remove();
						}
					} else if (item.getType() == ItemDesc.TYPE_DI) {
						if (readDi == 0) {
							iter.remove();
						}
					}
				}
				
				logger.debug(String.format("add group, name: %s, num: %d", group.getName(), items.size()));
				groups.add(group);
			}
		} catch (IOException e) {
			logger.error("load fail", e);
		}
		
		logger.debug(String.format("item_num: %d", groups.size()));
		
		return 0;
	}
	
	public ItemGroup[] listItemGroups() {
		return groups.toArray(new ItemGroup[0]);
	}

	public String getOpcSvrHost() {
		return opcSvrHost;
	}

	public void setOpcSvrHost(String opcSvrHost) {
		this.opcSvrHost = opcSvrHost;
	}

	public String getOpcSvrUser() {
		return opcSvrUser;
	}

	public void setOpcSvrUser(String opcSvrUser) {
		this.opcSvrUser = opcSvrUser;
	}

	public String getOpcSvrPasswd() {
		return opcSvrPasswd;
	}

	public void setOpcSvrPasswd(String opcSvrPasswd) {
		this.opcSvrPasswd = opcSvrPasswd;
	}

	public String getOpcProgId() {
		return opcProgId;
	}

	public void setOpcProgId(String opcProgId) {
		this.opcProgId = opcProgId;
	}
	
	public String getDwSvrHost() {
		return svrHost;
	}

	public void setDwSvrHost(String dwSvrHost) {
		this.svrHost = dwSvrHost;
	}

	public int getDwSvrPort() {
		return svrPort;
	}

	public void setDwSvrPort(int dwSvrPort) {
		this.svrPort = dwSvrPort;
	}
	
	public int getWorkNum() {
		return workNum;
	}

	public void setWorkNum(int workNum) {
		this.workNum = workNum;
	}
	
	public static void main(String[] args) {
		Config config = new Config();
		config.init();
	}
}
