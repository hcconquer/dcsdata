package cn.richinfo.dcsdata.client;

import java.util.Map;

import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.ItemState;

import cn.richinfo.dcsdata.opc.ItemGroup;

public abstract class DataListener {
	public abstract int handle(ItemGroup group, Map<Item, ItemState> rset);
	
	public int handle(Map<Item, ItemState> rset) {
		return handle(null, rset);
	}
}
