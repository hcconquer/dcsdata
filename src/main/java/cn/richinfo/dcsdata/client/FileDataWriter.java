package cn.richinfo.dcsdata.client;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.Map.Entry;

import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.ItemState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.richinfo.dcsdata.opc.ItemGroup;

public class FileDataWriter extends DataListener {
	private static final Logger logger = LoggerFactory.getLogger(FileDataWriter.class);
	
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	public int init() {
		File file = new File("/home/rdcs/dcsdata");
		if ((!file.exists()) || (!file.isDirectory()) || (!file.canWrite())) {
			return -1;
		}
		return 0;
	}
	
	private String buildFilePath(ItemGroup group) {
		return String.format("/home/rdcs/dcsdata/%s", group.getTable());
	}
	
	private String buildItemData(ItemGroup group, Item item, ItemState state) {
		Calendar cal = Calendar.getInstance();
		return String.format("%s%s%s", item.getId(), DATE_FORMAT.format(cal.getTime()), state.getValue().toString());
	}
	
	@Override
	public int handle(ItemGroup group, Map<Item, ItemState> rset) {
		String filePath = buildFilePath(group);
		String line = null;
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(new File(filePath)));
			for (Entry<Item, ItemState> entry: rset.entrySet()) {
				line = buildItemData(group, entry.getKey(), entry.getValue());
				writer.write(line);
				writer.newLine();
			}
		} catch (IOException e) {
			logger.error(String.format("write file fail, filepath: %s, line: %s", filePath, line));
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					logger.error(String.format("close file fail, filepath: %s", filePath));
				}
			}
		}
		return 0;
	}
}
