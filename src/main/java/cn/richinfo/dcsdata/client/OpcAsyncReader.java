package cn.richinfo.dcsdata.client;

import java.util.HashMap;
import java.util.Map;

import org.jinterop.dcom.common.JIException;
import org.openscada.opc.lib.da.AccessBase;
import org.openscada.opc.lib.da.DataCallback;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.ItemState;
import org.openscada.opc.lib.da.SyncAccess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.richinfo.dcsdata.opc.ItemGroup;

public class OpcAsyncReader extends OpcReader {
	private static final Logger logger = LoggerFactory.getLogger(OpcAsyncReader.class);
	
	AccessBase access = null;
	
	private int period = 1000;
	
	public OpcAsyncReader() {
	}
	
	public OpcAsyncReader(int period) {
		this.period = period;
	}
	
	@Override
	public int init() {
		int ret = super.init();
		if (ret < 0) {
			return ret;
		}
		try {
			access = new SyncAccess(server, period);
		} catch (Exception e) {
			logger.error("read fail", e);
			return -1;
		}
		return 0;
	}
	
	@Override
	public int read(ItemGroup group) {
		// final Map<Item, ItemState> rset = new ConcurrentHashMap<>();
		final ItemGroup fig = group;
		try {
			for (String tag: group.listTags()) {
				access.addItem(tag, new DataCallback() {
					@Override
					public void changed(Item item, ItemState state) {
						// logger.debug(String.format("%s", state.toString()));
						Map<Item, ItemState> rset = new HashMap<>();
						rset.put(item, state);
						for (DataListener listener: listeners) {
							listener.handle(fig, rset);
						}
					}
				});
			}
			access.bind();
			
//			while (terminated == 0) {
//				logger.debug("read data");
//				if (rset.size() > 0) {
//					for (DataListener listener: listeners) {
//						listener.handle(group, rset);
//					}
//					rset.clear();
//				}
//				Thread.sleep(period);
//			}
			Thread.sleep(6 * period);
//			if (rset.size() > 0) {
//				for (DataListener listener: listeners) {
//					listener.handle(group, rset);
//				}
//			}
		} catch (Exception e) {
			logger.error("read fail", e);
		} finally {
			try {
				access.unbind();
			} catch (JIException e) {
				logger.error("unbind fail", e);
			}
		}
		
		return 0;
	}
}
