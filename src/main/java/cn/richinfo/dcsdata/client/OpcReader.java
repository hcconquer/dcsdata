package cn.richinfo.dcsdata.client;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.da.AutoReconnectController;
import org.openscada.opc.lib.da.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.richinfo.dcsdata.opc.ItemGroup;

public abstract class OpcReader {
	private static final Logger logger = LoggerFactory.getLogger(OpcReader.class);
	
	protected int terminated = 0;
	
	protected String host = "222.222.223.113";
	protected String user = "Administrator";
	protected String passwd = "1";
	protected String progId = "Intellution.XOSOPC";
	
	protected AutoReconnectController controller;
	protected Server server;
	
	protected List<ItemGroup> groups = new ArrayList<>();
	protected List<DataListener> listeners = new ArrayList<>();
	
	public OpcReader() {
	}

	public int init() {
		return connect();
	}
	
	public int connect() {
		final ConnectionInformation ci = new ConnectionInformation();
		ci.setHost(host);
		ci.setUser(user);
		if (passwd != null) {
			ci.setPassword(passwd);
		}
		ci.setProgId(progId);
		
		server = new Server(ci, Executors.newSingleThreadScheduledExecutor());
		controller = new AutoReconnectController(server);
		
		try {
			server.connect();
			controller.connect();
		} catch (Exception e) {
			logger.error("connect to opc server fail", e);
			logger.error(String.format("host: %s, user: %s, passwd: %s, progid: %s", 
					host, user, passwd, progId));
			return -1;
		}
		
		logger.info("connect to opc server success");
		
		return 0;
	}
	
	public int close() {
		controller.disconnect();
		return 0;
	}
	
	public int read() {
		return read(groups.toArray(new ItemGroup[0]));
	}

	public abstract int read(ItemGroup group);
	
	public int read(ItemGroup[] groups) {
		for (ItemGroup group: groups) {
			read(group);
		}
		return 0;
	}
	
	public int addItemGroup(ItemGroup group) {
		groups.add(group);
		return 0;
	}
	
	public int addDataListener(DataListener listener) {
		listeners.add(listener);
		return 0;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getProgId() {
		return progId;
	}

	public void setProgId(String progId) {
		this.progId = progId;
	}
}
