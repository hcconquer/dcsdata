package cn.richinfo.dcsdata.client;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.openscada.opc.lib.da.Group;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.ItemState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.richinfo.dcsdata.opc.ItemGroup;
import cn.richinfo.dcsdata.opc.Quality;

public class OpcSyncReader extends OpcReader {
	private static final Logger logger = LoggerFactory.getLogger(OpcSyncReader.class);
	
	private Map<String, Object[]> mmap = new ConcurrentHashMap<>();
	
	@Override
	public int read(ItemGroup group) {
		logger.debug(String.format("start to read opc, group: %s", group.getName()));
		long begin = 0, end = 0;
		
		try {
			Object[] objs = mmap.get(group.getName());
			
			Group drp = null;
			Map<String, Item> tmap = null;
			Collection<Item> items = null;
			if (objs != null) {
				drp = (Group) objs[0];
				tmap = ((Map<String, Item>) objs[1]);
				items = (Collection<Item>) objs[2];
				
				logger.debug(String.format("find group in cache, name: %s", group.getName()));
			} else {
				begin = System.currentTimeMillis();
				
				drp = server.addGroup(group.getName());
				tmap = drp.addItems(group.listTags());
				items = tmap.values();
				mmap.put(group.getName(), new Object[] { drp, tmap, items });
				drp.setActive(true);
				
				end = System.currentTimeMillis();
				logger.debug(String.format("add group to server, name: %s, time: %dms", group.getName(), end - begin));
			}
			
			begin = System.currentTimeMillis();
			Map<Item, ItemState> rset = drp.read(false, items.toArray(new Item[0]));
			Map<Item, ItemState> fset = new HashMap<>();
			for (Entry<Item, ItemState> entry: rset.entrySet()) {
				Item item = entry.getKey();
				ItemState state = entry.getValue();
				// fset.put(item, state);
				if (state.getQuality() == Quality.OPC_QUALITY_GOOD) {
					fset.put(item, state);
				} else if (state.getQuality() == Quality.OPC_QUALITY_UNKNOWN) {
					fset.put(item, state);
				}
			}
			end = System.currentTimeMillis();
			
			logger.info(String.format("read opc success, group: %s, ori_num: %d, fix_num: %d, time: %dms",
					group.getName(), rset.size(), fset.size(), end - begin));
			
			if (fset.size() > 0) {
				for (DataListener listener: listeners) {
					listener.handle(group, fset);
				}
			}
		} catch (Exception e) {
			logger.error(String.format("read opc server fail, group_name: %s, reader: %s", group.getName(), toString()), e);
		}
		
		return 0;
	}
}
