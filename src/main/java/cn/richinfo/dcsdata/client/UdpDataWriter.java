package cn.richinfo.dcsdata.client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jboss.netty.bootstrap.ConnectionlessBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.channel.socket.DatagramChannelFactory;
import org.jboss.netty.channel.socket.nio.NioDatagramChannelFactory;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.ItemState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

import cn.richinfo.dcsdata.dto.GroupData;
import cn.richinfo.dcsdata.dto.ItemData;
import cn.richinfo.dcsdata.opc.ItemDesc;
import cn.richinfo.dcsdata.opc.ItemGroup;
import cn.richinfo.dcsdata.opc.OpcUtil;

public class UdpDataWriter extends DataListener {
	private static final Logger logger = LoggerFactory.getLogger(UdpDataWriter.class);
	
	private ConnectionlessBootstrap bootstrap;
	@SuppressWarnings("unused")
	private Channel channel;
	
	DatagramSocket sock;
	
	InetSocketAddress locAddr;
	private int locPort;
	
	InetSocketAddress dcSvrAddr;
	private String dcSvrHost;
	private int dcSvrPort;
	
	public UdpDataWriter() {
	}

	public UdpDataWriter(String dcHost, int dcPort) {
		this.dcSvrHost = dcHost;
		this.dcSvrPort = dcPort;
	}
	
	public int getLocPort() {
		return locPort;
	}

	public void setLocPort(int locPort) {
		this.locPort = locPort;
	}

	public String getDcHost() {
		return dcSvrHost;
	}

	public void setDcHost(String dcHost) {
		this.dcSvrHost = dcHost;
	}

	public int getDcPort() {
		return dcSvrPort;
	}

	public void setDcPort(int dcPort) {
		this.dcSvrPort = dcPort;
	}

	public int init() {
		locAddr = new InetSocketAddress("0.0.0.0", locPort);
		dcSvrAddr = new InetSocketAddress(dcSvrHost, dcSvrPort);
		
		DatagramChannelFactory channelFactory = new NioDatagramChannelFactory();
		bootstrap = new ConnectionlessBootstrap(channelFactory);
		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
			@Override
			public ChannelPipeline getPipeline() throws Exception {
				return Channels.pipeline(new SimpleChannelUpstreamHandler());
			}
		});
		
		try {
			channel = bootstrap.bind(locAddr);
		} catch (Exception e) {
			logger.error(String.format("bind fail"), e);
			return -1;
		}
		
		logger.debug(String.format("init success, dchost: %s, dcport: %d, locport: %d", 
				dcSvrHost, dcSvrPort, locPort));
		
		return 0;
	}

	public int write(byte[] data) {
		return write(data, dcSvrAddr);
	}
	
	public int write(byte[] data, InetSocketAddress dstAddr) {
		try {
			sock = new DatagramSocket();
			DatagramPacket packet = new DatagramPacket(data, data.length, dstAddr);
			sock.send(packet);
			sock.close();
		} catch (IOException e) {
			logger.error(String.format("send fail"), e);
			return -1;
		}
		
		logger.debug(String.format("send success, len: %d", data.length));
		
		return 0;
	}
	
	private byte[] buildGroupData(ItemGroup group, Map<Item, ItemState> rset) {
		GroupData data = new GroupData();
		
		if ((group == null) || (group.getTable() == null) || (group.getTable().length() <= 0)) {
			return null;
		}
		
		data.setName(group.getName());
		data.setTable(group.getTable());
		
		List<ItemData> items = new ArrayList<>();
		for (Entry<Item, ItemState> entry: rset.entrySet()) {
			Item item = entry.getKey();
			ItemState state = entry.getValue();
			
			ItemDesc desc = group.findItemByTag(item.getId());
			
			ItemData itda = new ItemData();
			itda.setNode(desc.getNode());
			itda.setValue(OpcUtil.valueOf(desc, item, state));
			
			items.add(itda);
		}
		data.setItems(items);
		
		String dstr = JSON.toJSONString(data);
		
		return dstr.getBytes();
	}
	
	@Override
	public int handle(ItemGroup group, Map<Item, ItemState> rset) {
		byte[] data = buildGroupData(group, rset);
		if (data == null) {
			return -1;
		}
		
		int ret = write(data);
		if (ret < 0) {
			logger.error(String.format("handle data, result: %d, group: %s, num: %d, len: %d", 
					ret, group.getName(), rset.size(), data.length));
			return -1;
		}
		logger.info(String.format("handle data, result: %d, group: %s, num: %d, len: %d", 
				ret, group.getName(), rset.size(), data.length));
		
		return ret;
	}
	
	public static void main(String[] args) {
		UdpDataWriter udpWriter = new UdpDataWriter();
		Config config = new Config();
		udpWriter.setDcHost(config.getDwSvrHost());
		udpWriter.setDcPort(config.getDwSvrPort());
		if (udpWriter.init() < 0) {
			logger.error("init fail");
			System.exit(-1);
		}
		for (;;) {
			udpWriter.write(new byte[] { 0x00 });
			try {
				Thread.sleep(0);
			} catch (InterruptedException e) {
				logger.debug("interrupted", e);
			}
		}
	}
}
