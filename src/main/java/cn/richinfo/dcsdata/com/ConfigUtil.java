package cn.richinfo.dcsdata.com;

import org.apache.commons.lang3.StringUtils;

import com.sun.tools.javac.util.Assert;

public class ConfigUtil {
	public static final int[] parsePorts(String str) {
		if (StringUtils.isEmpty(str)) {
			return null;
		}
		
		int idx = 0;
		
		/**
		 * ports=80:10
		 * start with 80 and num is 10
		 */
		idx = str.indexOf(':');
		if (idx >= 0) {
			String sport = str.substring(0, idx);
			String snum = str.substring(idx + 1);
			if (StringUtils.isEmpty(sport) || StringUtils.isEmpty(snum)) {
				return null;
			}
			
			int sp = Integer.parseInt(sport);
			int num = Integer.parseInt(snum);
			int[] ports = new int[num];
			for (int i = 0; i < num; i++) {
				ports[i] = sp + i;
			}
			return ports;
		}
		
		/**
		 * ports=80-89
		 * start with 80 to 89(include)
		 */
		idx = str.indexOf('-');
		if (idx >= 0) {
			String sport = str.substring(0, idx);
			String eport = str.substring(idx + 1);
			if (StringUtils.isEmpty(sport) || StringUtils.isEmpty(eport)) {
				return null;
			}
			
			int sp = Integer.parseInt(sport);
			int ep = Integer.parseInt(eport);
			int num = ep - sp + 1;
			int[] ports = new int[num];
			for (int i = 0; i < num; i++) {
				ports[i] = sp + i;
			}
			return ports;
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		Assert.check(ConfigUtil.parsePorts("80:10").length == 10);
		Assert.check(ConfigUtil.parsePorts("80-89").length == 10);
		System.out.println("Test OK");
	}
}
