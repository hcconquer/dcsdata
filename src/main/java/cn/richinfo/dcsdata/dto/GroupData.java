package cn.richinfo.dcsdata.dto;

import java.util.List;

public class GroupData {
	private String name;
	private String table;
	private String file;
	private List<ItemData> items;
	
	public GroupData() {
	}
	
	public GroupData(String name, List<ItemData> items) {
		this.name = name;
		this.items = items;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public List<ItemData> getItems() {
		return items;
	}

	public void setItems(List<ItemData> items) {
		this.items = items;
	}
}
