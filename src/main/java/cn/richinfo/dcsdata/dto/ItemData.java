package cn.richinfo.dcsdata.dto;

public class ItemData {
	private String node;
	private String value;
	
	public ItemData() {
	}
	
	public ItemData(String node, String value) {
		this.node = node;
		this.value = value;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
