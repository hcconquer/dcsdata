package cn.richinfo.dcsdata.opc;

import org.apache.commons.lang3.StringUtils;

public class ItemDesc {
	public static final int TYPE_AI = 0;
	public static final int TYPE_DI = 1;
	
	private String tag; // AI1:0
	private String gid; // A000000
	private String node; // 1HA10CT003
	private int type; // ref JIVariant
	
	public ItemDesc() {
	}
	
	public ItemDesc(String tag, String gid, String node, int type) {
		this.tag = tag;
		this.gid = gid;
		this.node = node;
		this.type = type;
	}
	
	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return String.format("tag: %s, gid: %s, node: %s, type: %d", tag, gid, node, type);
	}

	public String toLine() {
		return String.format("%s,%s,%s,%d", tag, StringUtils.trimToEmpty(gid), node, type);
	}
	
	public static void main(String[] args) {
		System.out.println(new ItemDesc().toString());
	}
}
