package cn.richinfo.dcsdata.opc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.util.TypeUtils;

public class ItemGroup {
	private static final Logger logger = LoggerFactory.getLogger(ItemGroup.class);
	
	private String name;
	private String table;
	private String file;
	private List<ItemDesc> items;
	
	public ItemGroup() {
	}
	
	public static ItemGroup load(String text) {
		ItemGroup group = JSON.parseObject(text, ItemGroup.class);
		return group;
	}
	
	public static ItemGroup load(File file) throws IOException {
		return load(FileUtils.readFileToString(file));
	}
	
	public String[] listTags() {
		List<String> list = new ArrayList<>();
		for (ItemDesc item: items) {
			list.add(item.getTag());
		}
		return list.toArray(new String[0]);
	}

	public ItemDesc findItemByTag(String tag) {
		for (ItemDesc item: items) {
			if (item.getTag().equals(tag)) {
				return item;
			}
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public List<ItemDesc> getItems() {
		return items;
	}

	public void setItems(List<ItemDesc> items) {
		this.items = items;
	}
	
	public static void main(String[] args) {
		TypeUtils.compatibleWithJavaBean = true;
		ItemGroup group = new ItemGroup();
		group.setName("test");
		List<ItemDesc> items = new ArrayList<>();
		items.add(new ItemDesc("AI1:0", "A000000", "1HA10CQ901", 0));
		items.add(new ItemDesc("AI1:0", "A000000", "1HA10CT001", 0));
		items.add(new ItemDesc("AI1:0", "A000000", "1HA10CT002", 0));
		group.setItems(items);
		String jstr = JSON.toJSONString(group, true);
		logger.debug(jstr);
	}
}
