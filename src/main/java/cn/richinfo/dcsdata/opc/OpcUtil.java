package cn.richinfo.dcsdata.opc;

import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.core.JIVariant;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.ItemState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpcUtil {
	private static final Logger logger = LoggerFactory.getLogger(OpcUtil.class);
	
	public static String valueOf(ItemDesc desc, Item item, ItemState state) {
		String valstr = null;
		JIVariant var = state.getValue();
		int itype = desc.getType();
		
		if (state.getQuality() == Quality.OPC_QUALITY_GOOD) {
			try {
				int type = var.getType();
				if (var.isNull()) {
				} else if (type == JIVariant.VT_EMPTY) {
					valstr = "";
				} else if (type == JIVariant.VT_BOOL) {
				} else if (type == JIVariant.VT_R4) {
					if (itype == ItemDesc.TYPE_AI) {
						valstr = Float.toString(var.getObjectAsFloat());
					} else if (itype == ItemDesc.TYPE_DI) {
						valstr = "1";
					}
				} else {
				}
			} catch (JIException e) {
				logger.error(String.format("parse value fail"), e);
			}
		} else if (state.getQuality() == Quality.OPC_QUALITY_UNKNOWN) {
			if (itype == ItemDesc.TYPE_DI) {
				valstr = "0";
			}
		}
		
		return valstr;
	}
}
