package cn.richinfo.dcsdata.opc;

public class Quality {
	public final static int OPC_QUALITY_GOOD = 0xC0;

	public final static int OPC_QUALITY_LOCAL_OVERRIDE = 0xD8;

	public final static int OPC_QUALITY_UNCERTAIN = 0x40;
	public final static int OPC_QUALITY_LAST_USABLE = 0x44;
	public final static int OPC_QUALITY_SENSOR_CAL = 0x50;
	public final static int OPC_QUALITY_EGU_EXCEEDED = 0x54;
	public final static int OPC_QUALITY_SUB_NORMAL = 0x58;

	public final static int OPC_QUALITY_BAD = 0x00;
	public final static int OPC_QUALITY_CONFIG_ERROR = 0x04;
	public final static int OPC_QUALITY_NOT_CONNECTED = 0x08;
	public final static int OPC_QUALITY_DEVICE_FAILURE = 0x0C;
	public final static int OPC_QUALITY_LAST_KNOWN = 0x14;
	public final static int OPC_QUALITY_COMM_FAILURE = 0x18;
	public final static int OPC_QUALITY_OUT_OF_SERVICE = 0x1C;
	public final static int OPC_QUALITY_SENSOR_FAILURE = 0x10;
	
	public final static int OPC_QUALITY_UNKNOWN = 0x1F; // I don't get its means, but read many error

	/**
	 * The OPC_LIMIT_xxx
	 */
	public final static int OPC_LIMIT_OK = 0x00;
	public final static int OPC_LIMIT_LOW = 0x01;
	public final static int OPC_LIMIT_HIGH = 0x02;
	public final static int OPC_LIMIT_CONST = 0x03;
}
