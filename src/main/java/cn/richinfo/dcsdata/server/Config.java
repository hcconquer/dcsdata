package cn.richinfo.dcsdata.server;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.richinfo.dcsdata.com.ConfigUtil;

public class Config {
	private static final Logger logger = LoggerFactory.getLogger(Config.class);
	
	private String path = "conf/server.properties";
	
	private String svrHost = "0.0.0.0";
//	private int svrPort = 30100;
	private int[] svrPorts = new int[] { 30100, 30101, 30102, 30103, 30104,
			30105, 30106, 30107, 30108, 30109 };
	
	private int workNum = 16;
	
	public Config() {
	}
	
	public Config(String path) {
		this.path = path;
	}
	
	public int init() {
		Properties props = new Properties();
		try {
			props.load(new FileReader(new File(path)));
		} catch (IOException e) {
			logger.error("load config fail", e);
		}
		
		String str = null;
		
		/* data center */
		svrHost = props.getProperty("dw_svr_host", svrHost);
		// svrPort = Integer.valueOf(props.getProperty("dc_svr_port", String.valueOf(svrPort)));
		str = props.getProperty("dw_svr_ports");
		if (str != null) {
			svrPorts = ConfigUtil.parsePorts(str);
		}
		
		return 0;
	}

	public String getSvrHost() {
		return svrHost;
	}

	public void setSvrHost(String dcHost) {
		this.svrHost = dcHost;
	}

//	public int getDwPort() {
//		return svrPort;
//	}
//
//	public void setDwPort(int dcPort) {
//		this.svrPort = dcPort;
//	}

	public int[] getSvrPorts() {
		return svrPorts;
	}

	public void setSvrPorts(int[] svrPorts) {
		this.svrPorts = svrPorts;
	}

	public int getWorkNum() {
		return workNum;
	}

	public void setWorkNum(int workNum) {
		this.workNum = workNum;
	}
}
