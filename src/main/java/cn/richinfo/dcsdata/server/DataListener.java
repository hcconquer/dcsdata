package cn.richinfo.dcsdata.server;

import cn.richinfo.dcsdata.dto.GroupData;

public abstract class DataListener {
	public abstract int handle(GroupData data);
}
