package cn.richinfo.dcsdata.server;

import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.alibaba.druid.pool.DruidPooledConnection;

import cn.richinfo.dcsdata.dto.GroupData;
import cn.richinfo.dcsdata.dto.ItemData;

public class DbDataWriter extends DataListener {
	private static final Logger logger = LoggerFactory.getLogger(DbDataWriter.class);
	
	private DruidDataSource dataSource;
	
	public DbDataWriter() {
	}

	public int init() {
		InputStream in = null;
		
		if (in == null) {
			in = getClass().getResourceAsStream("/jdbc.properties"); 
			if (in != null) {
				logger.info(String.format("load classpath db config succ"));
			}
		}
		
		if (in == null) {
			return -1;
		}

		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			logger.error(String.format("load config file fail"), e);
			return -1;
		}

		try {
			dataSource = (DruidDataSource) DruidDataSourceFactory.createDataSource(props);
		} catch (Exception e) {
			logger.error(String.format("create data source fail"), e);
			return -1;
		}

		logger.info(String.format("create data source success"));

		return 0;
	}
	
	public int destroy() {
		dataSource.close();
		
		logger.info(String.format("destroy data source success"));
		
		return 0;
	}

	public int write(GroupData data) {	
		DruidPooledConnection dpc = null;
		PreparedStatement stmt = null;
		try {			
			dpc = dataSource.getConnection();
			// dpc.setAutoCommit(false);
			stmt = dpc.prepareStatement(String.format("update %s set value = ?, realtime = now() where pnnode = ?", data.getTable()));
			int sum = 0;
			long begin, end;
			begin = System.currentTimeMillis();
			for (ItemData item: data.getItems()) {
				int index = 1;
				stmt.setString(index++, item.getValue());
				stmt.setString(index++, item.getNode());
				stmt.addBatch();
//				num = stmt.executeUpdate();
//				sum += num;
			}
			int[] nums = stmt.executeBatch();
			// dpc.commit();
			for (int num: nums) {
				sum += num;
			}
			end = System.currentTimeMillis();
			logger.info(String.format("update success, table: %s, group: %s, sum: %d, time: %dms", data.getTable(), data.getName(), sum, end - begin));
		} catch (SQLException e) {
			logger.error(String.format("write data to db fail"), e);
		} finally {	
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error(String.format("close statement fail"), e);
			}
			
			try {
				if (dpc != null) {
					dpc.close();
				}
			} catch (Exception e) {
				logger.error(String.format("close connection fail"), e);
			}
        }
		return 0;
	}
	
	private int updateState(DruidPooledConnection dpc, String node, int value) throws SQLException {
		PreparedStatement stmt = null;
		stmt = dpc.prepareStatement("update rdcs_jgs.jgs_realtime set value=?, realtime = now() where pnnode=?");
		stmt.setInt(1, value);
		stmt.setString(2, String.format("%s%s", node, "STT"));
		int num = stmt.executeUpdate();
		logger.debug(String.format("update state succ, node: %s, value: %d, num: %d", node, value, num));
		return num;
	}
	
	public void updateStates() {
		DruidPooledConnection dpc = null;
		PreparedStatement stmt = null;
		ResultSet rset;
		int rval = 0, num = 0;
		try {			
			dpc = dataSource.getConnection();
			stmt = dpc.prepareStatement(String.format("select r.mkey, v.pnnode, v.`value`, r.rval from rdcs_jgs.jgs_realtime as v, rdcs_jgs.jgs_relation as r where v.pnnode = r.skey AND v.`value`=1"));
			rset = stmt.executeQuery();
			while (rset.next()) {
				String mkey = rset.getString("mkey");
				rval = rset.getInt("rval");
				num = updateState(dpc, mkey, rval);
				if (num <= 0) {
					logger.error(String.format("update state fail, num: %d", num));
				}
			}
		} catch (SQLException e) {
			logger.error(String.format("write data to db fail"), e);
		} finally {	
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				logger.error(String.format("close statement fail"), e);
			}
			
			try {
				if (dpc != null) {
					dpc.close();
				}
			} catch (Exception e) {
				logger.error(String.format("close connection fail"), e);
			}
        }
	}
	
	@Override
	public int handle(GroupData data) {
		return write(data);
	}

	public static void main(String[] args) {
		final DbDataWriter writer = new DbDataWriter();
		writer.init();
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					writer.updateStates();
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						logger.info("interrupted", e);
					}
				}
			}
		}).start();
	}
}
