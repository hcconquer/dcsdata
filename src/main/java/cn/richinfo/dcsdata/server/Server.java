package cn.richinfo.dcsdata.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Server {
	private static final Logger logger = LoggerFactory.getLogger(Server.class);
	
	private Config config;
	
	private int terminated = 0; // 
	
	private UdpDataReader[] udpReaders;
	
	private DbDataWriter dbWriter;
	
	public int initConfig() {
		config = new Config();
		return config.init();
	}
	
	public int initUdpReader() {
		int ret = 0;
		int num = config.getSvrPorts().length;
		udpReaders = new UdpDataReader[num];
		for (int idx = 0; idx < num; idx++) {
			int port = config.getSvrPorts()[idx];
			UdpDataReader udpReader = new UdpDataReader();
			udpReader.setSvrHost(config.getSvrHost());
			udpReader.setDcPort(port);
			ret = udpReader.init();
			if (ret == 0) {
				logger.info(String.format("init succ, index: %d, host: %s, port: %d", idx, config.getSvrHost(), port));
			} else {
				logger.error(String.format("init fail, index: %d, host: %s, port: %d", idx, config.getSvrHost(), port));
				return -1;
			}
			
			udpReader.addDataListener(dbWriter);
			udpReaders[idx] = udpReader;
		}
		
		return 0;
	}
	
	public int initDbWriter() {
		dbWriter = new DbDataWriter();
		return dbWriter.init();
	}
	
	public int init() {
		int ret = 0;
		
		ret = initConfig();
		if (ret < 0) {
			logger.error("init config fail");
			return -1;
		}
		
		ret = initDbWriter();
		if (ret < 0) {
			logger.error("init db writer fail");
			return -1;
		}
		
		ret = initUdpReader();
		if (ret < 0) {
			logger.error("init udp reader fail");
			return -1;
		}
		
//		udpReader.addDataListener(dbWriter);
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (terminated == 0) {
					dbWriter.updateStates();
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						logger.info("interrupted", e);
						terminated = 1;
					}
				}
			}
		}).start();
		
		logger.info("init success");
		
		return 0;
	}
	
	public int destroy() {
		dbWriter.destroy();
		
		logger.info("destroy success");
		
		return 0;
	}
	
	public int handle() {
		while (terminated == 0) {
			logger.debug("handle");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.info("interrupted", e);
				terminated = 1;
			}
		}
		return 0;
	}
	
	public static void main(String[] args) {
//		PropertyConfigurator.configure("conf/log4j.properties");
		Server server = new Server();
		int ret = server.init();
		if (ret < 0) {
			logger.error(String.format("init fail, result: %d", ret));
			System.exit(-1);
		}
		server.handle();
		server.destroy();
	}
}
