package cn.richinfo.dcsdata.server;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ConnectionlessBootstrap;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.FixedReceiveBufferSizePredictorFactory;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.channel.socket.DatagramChannelFactory;
import org.jboss.netty.channel.socket.nio.NioDatagramChannelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

import cn.richinfo.dcsdata.dto.GroupData;

public class UdpDataReader {
	private static final Logger logger = LoggerFactory.getLogger(UdpDataReader.class);
	
	private ConnectionlessBootstrap bootstrap;
	
	private List<DataListener> listeners = new ArrayList<>();
	
	InetSocketAddress svrAddr;
//	InetSocketAddress ldcSockAddr;
	
	private String svrHost;
	private int svrPort;
	
	private ExecutorService thpool;
	
	public UdpDataReader() {
	}

	public UdpDataReader(String dcHost, int dcPort) {
		this.svrHost = dcHost;
		this.svrPort = dcPort;
	}
	
	public String getSvrHost() {
		return svrHost;
	}

	public void setSvrHost(String dcHost) {
		this.svrHost = dcHost;
	}

	public int getDcPort() {
		return svrPort;
	}

	public void setDcPort(int dcPort) {
		this.svrPort = dcPort;
	}
	
	private int handleUdpRequest(ChannelHandlerContext ctx, MessageEvent e) {
		ChannelBuffer buffer = (ChannelBuffer) e.getMessage();
		byte[] buf = buffer.copy().array();
		logger.debug(String.format("recv, len: %d", buf.length));
		
		String dstr = new String(buf);
		final GroupData data = JSON.parseObject(dstr, GroupData.class);
		logger.debug(String.format("name: %s, table: %s, node_num: %d", data.getName(), data.getTable(), data.getItems().size()));
		
		for (final DataListener listener: listeners) {
			thpool.submit(new Runnable() {
	            @Override
	            public void run() {
	    			listener.handle(data);
	            }
	        });
		}
		
		return 0;
	}

	public int init() {
		svrAddr = new InetSocketAddress(svrHost, svrPort);
		
		DatagramChannelFactory channelFactory = new NioDatagramChannelFactory();
		bootstrap = new ConnectionlessBootstrap(channelFactory);
		bootstrap.setOption("receiveBufferSizePredictorFactory", new FixedReceiveBufferSizePredictorFactory(65535));
		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
			@Override
			public ChannelPipeline getPipeline() throws Exception {
				return Channels.pipeline(new SimpleChannelUpstreamHandler() {
					@Override
				    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
				    	logger.error(String.format("udp error"), e);
				    }
					
					@Override
					public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
						super.messageReceived(ctx, e); 
						handleUdpRequest(ctx, e);
					}
				});
			}
		});
		
		try {
			bootstrap.bind(svrAddr);
		} catch (Exception e) {
			logger.error(String.format("bind fail"), e);
			return -1;
		}
		
		logger.debug(String.format("init success, host: %s, port: %d", svrHost, svrPort));
		
		thpool = Executors.newFixedThreadPool(64);
		
		return 0;
	}
	
	public int addDataListener(DataListener listener) {
		listeners.add(listener);
		return 0;
	}
	
	public static void main(String[] args) {
		UdpDataReader udpReader = new UdpDataReader();
		Config config = new Config();
		udpReader.setSvrHost(config.getSvrHost());
		udpReader.setDcPort(config.getSvrPorts()[0]);
		if (udpReader.init() < 0) {
			logger.error("init fail");
			System.exit(-1);
		}
		for (;;) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.debug("interrupted", e);
			}
		}
	}
}
