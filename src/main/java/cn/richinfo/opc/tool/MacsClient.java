package cn.richinfo.opc.tool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacsClient extends OpcClient {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(MacsClient.class);
	
	public MacsClient() {
		host = "130.0.0.55";
		user = "Administrator";
		passwd = null;
		progId = "Hollysys.MACSV5OPCServer.1";
		tags = null;
	}
	
	public static void main(String[] args) throws Exception {
		MacsClient client = new MacsClient();
		client.praseOpts(args);
		client.readAllTags();
	}
}