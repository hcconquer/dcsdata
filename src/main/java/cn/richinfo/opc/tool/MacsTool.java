package cn.richinfo.opc.tool;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

import cn.richinfo.dcsdata.opc.ItemDesc;
import cn.richinfo.dcsdata.opc.ItemGroup;

public class MacsTool {	
	private static final Logger logger = LoggerFactory.getLogger(MacsTool.class);
	
	public void writeTagNodeMap(String jpath, String path, int type) throws IOException {
		// List<ItemDesc> items = new ArrayList<>();
		List<String> lines = FileUtils.readLines(new File(path));
		List<String> tlines = new ArrayList<String>();
		String node = null;
		for (String line: lines) {
			node = StringUtils.trim(line);
			
			ItemDesc item = new ItemDesc();
			item.setTag(node);
			item.setGid(null);
			item.setNode(node);
			item.setType(type);
			
			tlines.add(item.toLine());
			// items.add(item);
		}
		
		File jfile = new File(jpath);
		FileUtils.writeLines(jfile, tlines, true);
		
		logger.info(String.format("write to %s succ", jfile.getAbsolutePath()));
	}
	
	public void writeMuchItemJson(String jpath, String ipath) throws IOException {
		List<String> lines = FileUtils.readLines(new File(ipath));
		List<ItemDesc> items = new ArrayList<>();
		String[] cols = null;
		int num = 1;
		for (String line: lines) {
			cols = line.split(",");
			if ((cols == null) || (cols.length <= 0)) {
				continue;
			}
			
			ItemDesc item = new ItemDesc();
			item.setTag(cols[0]);
			item.setGid(cols[1]);
			item.setNode(cols[2]);
			item.setType(Integer.parseInt(cols[3]));
			
			if (item.getType() == ItemDesc.TYPE_DI) {
//				continue;
			}
			
			items.add(item);
			
			if (items.size() >= 50) {
				ItemGroup group = new ItemGroup();
				String name = String.format("yc3_realtime_%03d", num);
				group.setName(name);
				group.setTable("rdcs_yc3.yc3_realtime");
				group.setFile(name);
				group.setItems(items);
				String jstr = JSON.toJSONString(group, true);
				File file = new File(name + ".json");
				FileUtils.writeStringToFile(file, jstr);
				logger.info(file.toString());
				
				items.clear();
				
				num++;
			}
		}
	}
	
	public static void main(String[] args) {
		MacsTool tool = new MacsTool();
		try {
			FileUtils.deleteQuietly(new File("tmap.txt"));
//			tool.writeTagNodeMap("tmap.txt", "src/main/config/raw/hj/ai.txt", ItemDesc.TYPE_AI);
//			tool.writeTagNodeMap("tmap.txt", "src/main/config/raw/hj/di.txt", ItemDesc.TYPE_DI);
			tool.writeTagNodeMap("tmap.txt", "src/main/config/raw/yc3/all.txt", ItemDesc.TYPE_AI);
			tool.writeMuchItemJson("demo.json", "tmap.txt");
		} catch (IOException e) {
			logger.error("fail", e);
		}
	}
}
