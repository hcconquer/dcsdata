package cn.richinfo.opc.tool;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.core.JIVariant;
import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.da.Group;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.ItemState;
import org.openscada.opc.lib.da.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpcClient {
	private static final Logger logger = LoggerFactory.getLogger(OpcClient.class);
	
	private static final int CMD_READ_ALL_TAG = 1;
	private static final int CMD_READ_TAGS = 2;
	
	protected int cmd = 0;
	protected String host = null;
	protected String user = null;
	protected String passwd = null;
	protected String progId = null;
	protected String tags = null;

	protected int PrintUsage(String[] args) {
		System.out.printf("Usage: %s [OPTION]...", args[0]);
		System.out.printf("  %3s %-10s %s\n", "-c", "--cmd", "cmd");
		System.out.printf("  %3s %-10s %s\n", "-h", "--host", "host");
		System.out.printf("  %3s %-10s %s\n", "-u", "--user", "user");
		System.out.printf("  %3s %-10s %s\n", "-p", "--progid", "progid");
		System.out.printf("  %3s %-10s %s\n", "-t", "--tag", "tag");
		return 0;
	}
	
	protected int praseOpts(String[] args) {
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("c", "cmd", true, "cmd, read_all_tags, read_tags");
		options.addOption("h", "host", true, "host");
		options.addOption("u", "user", true, "user");
		options.addOption("p", "passwd", true, "passwd");
		options.addOption("g", "progid", true, "progid");
		options.addOption("t", "tag", true, "tag");
		CommandLine cl = null;
		try {
			cl = parser.parse(options, args);
		} catch (ParseException e) {
			logger.debug("parse command line fail", e);
			return -1;
		}
		if (cl.hasOption('c')) {
			String s = cl.getOptionValue('c');
			if (s.compareToIgnoreCase("read_all_tags") == 0) {
				cmd = CMD_READ_ALL_TAG;
			} else if (s.compareToIgnoreCase("read_tags") == 0) {
				cmd = CMD_READ_TAGS;
			}
		}
		if (cl.hasOption('h')) {
			host = cl.getOptionValue('h');
		}
		if (cl.hasOption('u')) {
			user = cl.getOptionValue('u');
		}
		if (cl.hasOption('p')) {
			passwd = cl.getOptionValue('p');
		}
		if (cl.hasOption('g')) {
			progId = cl.getOptionValue('g');
		}
		if (cl.hasOption('t')) {
			tags = cl.getOptionValue('t');
		}
		logger.debug(String.format("cmd: %d, host: %s, user: %s, passwd: %s, progId: %s, tag: %s", 
				cmd, host, user, passwd, progId, tags));
		
		return 0;
	}
	
	private String parseItem(Item item, ItemState state) throws JIException {
		String valstr = null;
		JIVariant var = state.getValue();
		String id = item.getId();
		int type = var.getType();
		logger.debug(state.toString());
		if (var.isNull()) {
			valstr = String.format("id: %s, type: %d, value: %s", id, type, "null");
		} else if (type == JIVariant.VT_EMPTY) {
			valstr = String.format("id: %s, type: %d, value: %s", id, type, "empty");
		} else if (type == JIVariant.VT_BOOL) {
			valstr = String.format("id: %s, type: %d, value: %s", id, type, var.getObjectAsBoolean());
		} else if (type == JIVariant.VT_R4) {
			valstr = String.format("id: %s, type: %d, value: %s", id, type, var.getObjectAsFloat());
		} else {
			valstr = String.format("id: %s, type: %d, value: %s", id, type, var.getObjectAsComObject());
		}
		
		return valstr;
	}
	
	public void readTags(String[] tags) throws Exception {
		logger.debug(String.format("host: %s, user: %s, progid: %s", host, user, progId));
		
		// create connection information
		final ConnectionInformation ci = new ConnectionInformation();
		ci.setHost(host);
		ci.setUser(user);
		if (passwd != null) {
			ci.setPassword(passwd);
		}
		ci.setProgId(progId);

		// create a new server
		final Server server = new Server(ci, Executors.newSingleThreadScheduledExecutor());

		Group group = null;
		try {
			server.connect();
			
			group = server.addGroup("rdcs");
			group.setActive(true);
			
			Map<String, Item> itemmap = group.addItems(tags);
			Collection<Item> items = itemmap.values();
			
			for (;;) {
				Map<Item, ItemState> rset = group.read(false, items.toArray(new Item[0]));
				for (Entry<Item, ItemState> entry: rset.entrySet()) {
					logger.debug(parseItem(entry.getKey(), entry.getValue()));
				}
				
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			logger.error("connect to opc server fail", e);
		}
	}
	
	public void readAllTags() throws Exception {
		readTags(null);
	}
	
	public void readTagsFromFile(String path) throws Exception {
		String line = null;
		String[] tags = null;
		List<String> taglist = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(path));
		while ((line = br.readLine()) != null) {
			tags = line.split(",");
			if ((tags == null) || (tags.length <= 0)) {
				continue;
			}
			taglist.add(tags[0]);
		}
		br.close();
		readTags(taglist.toArray(new String[0]));
	}
	
	public static void main(String[] args) throws Exception {
		OpcClient client = new OpcClient();
		client.praseOpts(args);
//		 client.readAllTags();
//		client.readTags(new String[] { 
//				"AI1:624"/*A000208*/, 
//				"AI2:75"/*A000275*/, 
//				"AI9:579"/*A002193*/,
//				"AI7:252"/*A001584*/
//		});
		if (client.cmd == CMD_READ_ALL_TAG) {
			client.readAllTags();
		} else if (client.cmd == CMD_READ_TAGS) {
			String[] ts = client.tags.split(",");
			client.readTags(ts);
			/* AI1:624,DI1:0,DI1:1,DI1:2,DI1:656,DI1:657,DI1:658,DI1:659,DI1:660 */
//			new String[] { 
//					"AI1:624"/*A000208*/,
//					"DI1:0"/*D000007,1HG11N001ZS*/,
//					"DI1:3",
//					"DI1:5",
//					"DI3:656",
//					"DI3:657",
//					"DI3:658",
//					"DI3:659",
//					"DI3:660",/*D002660*/
//			});
		}
//		 client.readTagsFromFile("tpmap.txt");
	}
}
