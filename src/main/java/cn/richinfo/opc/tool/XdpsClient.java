package cn.richinfo.opc.tool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XdpsClient extends OpcClient {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(XdpsClient.class);
	
	public XdpsClient() {
		host = "222.222.223.113";
		user = "Administrator";
		passwd = "1";
		progId = "Intellution.XOSOPC";
		tags = "";
	}

	public static void main(String[] args) throws Exception {
		XdpsClient client = new XdpsClient();
		client.praseOpts(args);
//		 client.readAllTags();
//		client.readTags(new String[] { 
//				"AI1:624"/*A000208*/, 
//				"AI2:75"/*A000275*/, 
//				"AI9:579"/*A002193*/,
//				"AI7:252"/*A001584*/
//		});
		client.readTags(new String[] { 
//				"AI1:624"/*A000208*/,
//				"DI1:0"/*D000007,1HG11N001ZS*/,
//				"DI1:3",
//				"DI1:5",
				"DI3:656",
				"DI3:657",
				"DI3:658",
				"DI3:659",
				"DI3:660",/*D002660*/
		});
//		 client.readTagsFromFile("tpmap.txt");
	}
}