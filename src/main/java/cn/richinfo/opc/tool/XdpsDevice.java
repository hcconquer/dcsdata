package cn.richinfo.opc.tool;

public class XdpsDevice {
	public static final int DATA_TYPE_AI = 0;
	public static final int DATA_TYPE_DI = 1;
	
	private String device;
	private String name;
	private String description;
	private int startAddress;
	private int length;
	private int primaryPollTime;
	private String secondaryPollTime;
	private int phase;
	private String accessTime;
	private int deadBand;
	private int enabled;
	private int latchData;
	private int outputDisabled;
	private int blockWritesEnabled;
	private int dataType;
	private int pointType;
	private int startPointID;
	private int pointNum;

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStartAddress() {
		return startAddress;
	}

	public void setStartAddress(int startAddress) {
		this.startAddress = startAddress;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getPrimaryPollTime() {
		return primaryPollTime;
	}

	public void setPrimaryPollTime(int primaryPollTime) {
		this.primaryPollTime = primaryPollTime;
	}

	public String getSecondaryPollTime() {
		return secondaryPollTime;
	}

	public void setSecondaryPollTime(String secondaryPollTime) {
		this.secondaryPollTime = secondaryPollTime;
	}

	public int getPhase() {
		return phase;
	}

	public void setPhase(int phase) {
		this.phase = phase;
	}

	public String getAccessTime() {
		return accessTime;
	}

	public void setAccessTime(String accessTime) {
		this.accessTime = accessTime;
	}

	public int getDeadBand() {
		return deadBand;
	}

	public void setDeadBand(int deadBand) {
		this.deadBand = deadBand;
	}

	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}

	public int getLatchData() {
		return latchData;
	}

	public void setLatchData(int latchData) {
		this.latchData = latchData;
	}

	public int getOutputDisabled() {
		return outputDisabled;
	}

	public void setOutputDisabled(int outputDisabled) {
		this.outputDisabled = outputDisabled;
	}

	public int getBlockWritesEnabled() {
		return blockWritesEnabled;
	}

	public void setBlockWritesEnabled(int blockWritesEnabled) {
		this.blockWritesEnabled = blockWritesEnabled;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public int getPointType() {
		return pointType;
	}

	public void setPointType(int pointType) {
		this.pointType = pointType;
	}

	public int getStartPointID() {
		return startPointID;
	}

	public void setStartPointID(int startPointID) {
		this.startPointID = startPointID;
	}

	public int getPointNum() {
		return pointNum;
	}

	public void setPointNum(int pointNum) {
		this.pointNum = pointNum;
	}
}
