package cn.richinfo.opc.tool;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

import cn.richinfo.dcsdata.opc.ItemDesc;
import cn.richinfo.dcsdata.opc.ItemGroup;

public class XdpsTool {
	@SuppressWarnings("unused")
	private static final int SECTION_CHANNEL = 3;
	private static final int SECTION_DEVICE = 4;
	
	private static final int SECTION_AX = 3;
	private static final int SECTION_DX = 4;
	
	private static final int POINT_LEN_MIN = 5;
	
	private static final Logger logger = LoggerFactory.getLogger(XdpsTool.class);
	
	private List<XdpsDevice> readDevices(String path) throws IOException {
		List<XdpsDevice> list = new ArrayList<XdpsDevice>();
		
		File file = new File(path);
		logger.debug(file.getAbsolutePath());
		
//		String line = null;
		int section = 0;
		
		List<String> lines = FileUtils.readLines(file);
		
//		BufferedReader br = new BufferedReader(new FileReader(file));
//		while ((line = br.readLine()) != null) {
		for (String line: lines) {
			if (line.startsWith("#Device")) {
				section = SECTION_DEVICE;
				continue;
			}
			if (section == SECTION_DEVICE) {
				String[] array = line.split(",");
				XdpsDevice device = new XdpsDevice();
				device.setDevice(array[0]);
				device.setLength(Integer.parseInt(array[4]));
				device.setDataType(Integer.parseInt(array[15]));
				device.setStartPointID(Integer.parseInt(array[16]));
				device.setPointNum(Integer.parseInt(array[17]));
				logger.debug(String.format("device: %s, length: %d, type: %d, start: %d, num: %d", 
						device.getDevice(), device.getLength(), device.getDataType(), device.getStartPointID(), device.getPointNum()));
				list.add(device);
			}
		}
//		br.close();
		
		return list;
	}
	
	private Map<String, String> readNodes(String path) throws IOException {
		Map<String, String> map = new HashMap<String, String>();
		
		File file = new File(path);
		logger.debug(file.getAbsolutePath());
		
//		String line = null;
		int section = 0, ainum = 0, dinum = 0;
//		BufferedReader br = new BufferedReader(new FileReader(file));
//		while ((line = br.readLine()) != null) {
		List<String> lines = FileUtils.readLines(file);
		for (String line: lines) {
			if (line.startsWith("BEGIN_AX")) {
				section = SECTION_AX;
				continue;
			} else if (line.startsWith("END_AX")) {
				section = 0;
				continue;
			} else if (line.startsWith("BEGIN_DX")) {
				section = SECTION_DX;
				continue;
			} else if (line.startsWith("END_DX")) {
				section = 0;
				continue;
			} 

			int seq = 0;
			String seqstr = null;
			
			if (section == SECTION_AX) {
				seq = ainum++;
				seqstr = String.format("A%06d", seq);
			} else if (section == SECTION_DX) {
				seq = dinum++;
				seqstr = String.format("D%06d", seq);
			}
				
			if (seqstr != null) {
				String[] array = line.split(",");

				String pnode = array[2];
				// logger.debug(String.format("pnode: %s, index: %d", pnode, seq));

				map.put(seqstr, pnode);
			}
		}
//		br.close();
		
		return map;
	}
	
	private void writeTagNodeMap(String mpath, String opath, String ppath) throws IOException {
		// XdpsTool conv = new XdpsTool();
		List<XdpsDevice> devices = readDevices(opath);
		Map<String, String> points = readNodes(ppath);
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(mpath)));
		
		for (XdpsDevice device: devices) {
			int itlen = device.getLength() / device.getPointNum();
			int pos = 0, seq = 0;
			String pnode = null, seqstr = null;
			
//			logger.debug(device.getDevice() + " " + device.getStartPointID() + 
//					" " + device.getPointNum() + " " + device.getDataType() + 
//					" " + device.getLength() + " " + itlen);
			
			for (int i = 0; i < device.getPointNum(); i++) {
				pos = i * itlen;
				seq = device.getStartPointID() + i;
				if (device.getDataType() == XdpsDevice.DATA_TYPE_AI) {
					seqstr = String.format("A%06d", seq);
				} else if (device.getDataType() == XdpsDevice.DATA_TYPE_DI) {
					seqstr = String.format("D%06d", seq);
				}
				pnode = points.get(seqstr);
				if (pnode != null) {
					String line = String.format("%s:%d,%s,%s,%d", device.getDevice(), pos, seqstr, pnode, device.getDataType());
					// logger.debug(line);
					bw.write(line);
					bw.newLine();
				} else {
					// logger.warn(String.format("point is not exist, seq: %s", seqstr));
				}
			}
		}
		
		bw.close();
	}
	
	private String readNodeFromString(String str) {
		// logger.debug(str);
		int num = 0;
		while (num < str.length()) {
			char c = str.charAt(num);
			// if (Character.isUpperCase(c) && Character.isDigit(c)) {
			if (Character.isUpperCase(c) || Character.isDigit(c)) {
				num++;
			} else {
				break;
			}
		}
		if (num < POINT_LEN_MIN) {
			return null;
		}
		return str.substring(0, num);
	}
	
	private Map<String, Integer> readNodesFromFig(String path) throws IOException {
		Map<String, Integer> map = new HashMap<String, Integer>();
		BufferedReader br = new BufferedReader(new FileReader(new File(path)));
		String line = null, point = null;
		while ((line = br.readLine()) != null) {
			// line = "D1P151B1^F$01FB";
			// logger.debug(line);
			int idx = 0;
			while (idx < line.length()) {
				// logger.debug(String.format("index: %d", idx));
				point = readNodeFromString(line.substring(idx));
				if (point != null) {
					// logger.debug(String.format("point: %s, len: %d", point, point.length()));
					idx += point.length();
					// logger.debug(String.format("index: %d", idx));
					Integer num = map.get(point);
					if (num == null) {
						num = 1;
					} else {
						num += 1;
					}
					map.put(point, num);
				} else {
					idx++;
					// logger.debug(String.format("index: %d", idx));
				}
			}
			// break;
		}
		br.close();
		
		logger.debug(String.format("read from fig, num: %d", map.size()));
//		for (Entry<String, Integer> entry: map.entrySet()) {
//			// logger.debug(String.format("point: %s, num: %d", entry.getKey(), entry.getValue()));
//		}
		
		return map;
	}
	
	public void writeFigTagNodeMap(String mpath, String fig, String tmap) throws IOException {
		Map<String, Integer> nodes = readNodesFromFig(fig);
//		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(mpath)));
//		BufferedReader br = new BufferedReader(new FileReader(new File(tmap)));
//		String line = null;
		String[] cols = null;
//		while ((line = br.readLine()) != null) {
		List<String> ilines = new ArrayList<>();
		List<String> lines = FileUtils.readLines(new File(tmap));
		for (String line: lines) {
			for (String node: nodes.keySet()) {
				cols = line.split(",");
				if ((cols == null) || (cols.length <= 0)) {
					continue;
				}
				// logger.debug(node + "---" + cols[2]);
				if (node.compareToIgnoreCase(cols[2]) == 0) {
					// logger.debug(String.format("%s", line));
					ilines.add(line);
//					bw.write(line);
//					bw.newLine();
				}	
			}
		}
//		br.close();
//		bw.close();
		FileUtils.writeLines(new File(mpath), ilines);
	}
	
	public void writeFigTagNodeMapJson(String jpath, String mpath) throws IOException {
		ItemGroup group = new ItemGroup();
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(jpath)));
		BufferedReader br = new BufferedReader(new FileReader(new File(mpath)));
		String line = null;
		String[] cols = null;
		List<ItemDesc> items = new ArrayList<>();
		while ((line = br.readLine()) != null) {
			cols = line.split(",");
			if ((cols == null) || (cols.length <= 0)) {
				continue;
			}
			
			ItemDesc item = new ItemDesc();
			item.setTag(cols[0]);
			item.setGid(cols[1]);
			item.setNode(cols[2]);
			item.setType(Integer.parseInt(cols[3]));
			
			items.add(item);
		}
		group.setItems(items);
		String jstr = JSON.toJSONString(group, true);
		logger.debug(jstr);
		bw.write(jstr);
		br.close();
		bw.close();
	}
	
	public void writeMuchItemJson(String jpath, String ipath) throws IOException {
		List<String> lines = FileUtils.readLines(new File(ipath));
		List<ItemDesc> items = new ArrayList<>();
		String[] cols = null;
		int num = 1;
		for (String line: lines) {
			cols = line.split(",");
			if ((cols == null) || (cols.length <= 0)) {
				continue;
			}
			
			ItemDesc item = new ItemDesc();
			item.setTag(cols[0]);
			item.setGid(cols[1]);
			item.setNode(cols[2]);
			item.setType(Integer.parseInt(cols[3]));
			
			if (item.getType() == ItemDesc.TYPE_DI) {
//				continue;
			}
			
			items.add(item);
			
			if (items.size() >= 50) {
				ItemGroup group = new ItemGroup();
				String name = String.format("jgs_realtime_%03d", num);
				group.setName(name);
				group.setTable("rdcs_jgs.jgs_realtime");
				group.setFile(name);
				group.setItems(items);
				String jstr = JSON.toJSONString(group, true);
				File file = new File(name + ".json");
				FileUtils.writeStringToFile(file, jstr);
				logger.info(file.toString());
				
				items.clear();
				
				num++;
			}
		}
	}
	
	private void writeRelation(String rpath, String ppath) throws IOException {
		List<String[]> seglist = new ArrayList<>();
		List<String> lines = FileUtils.readLines(new File(ppath));
		for (String line: lines) {
			String[] segs = line.split(",");
			if (ArrayUtils.getLength(segs) < 4) {
				continue;
			}
			seglist.add(segs);
		}

		lines.clear();
		
		for (int i = 0; i < seglist.size(); i++) {
			String[] segs = seglist.get(i);
			for (int j = 0; j < seglist.size(); j++) {
				if (j == i) {
					continue;
				}
				String[] ss = seglist.get(j);
				
				String line = null;
				if (StringUtils.contains(ss[3], segs[3] + "运行") || StringUtils.contains(ss[3], segs[3] + "开状态")) {
					line = String.format("%s,%s,%s,%s,1", segs[2], ss[2], segs[3], ss[3]);
				} else if (StringUtils.contains(ss[3], segs[3] + "停止") || StringUtils.contains(ss[3], segs[3] + "关状态")) {
					line = String.format("%s,%s,%s,%s,2", segs[2], ss[2], segs[3], ss[3]);
				} else if (StringUtils.contains(ss[3], segs[3] + "故障")) {
					line = String.format("%s,%s,%s,%s,4", segs[2], ss[2], segs[3], ss[3]);
				}
				
				if (line != null) {
//					logger.debug(line);
					lines.add(line);
				}
			}
		}
		
		FileUtils.writeLines(new File(rpath), lines);
	}
	
	public static void main(String[] args) {
		XdpsTool tool = new XdpsTool();
		try {
//			tool.writeTagNodeMap("tmap.txt", "src/main/config/raw/jgs/xdps_opc.csv", "src/main/config/raw/jgs/Pointdir.cfg");
//			// tool.writeFigTagNodeMap("demo.txt", "demo.fig", "tmap.txt");
//			tool.writeMuchItemJson("demo.json", "tmap.txt");
			
			tool.writeRelation("rmap.txt", "src/main/config/raw/jgs/Pointdir.cfg");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
